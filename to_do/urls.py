from django.contrib import admin
from django.urls import include, path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from rest_framework import routers, permissions
from activity.viewsets import ToDoViewSet


schema_view = get_schema_view(
   openapi.Info(
      title="To Do API",
      default_version='v1',
      description="To Do: Activities List",
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


router = routers.DefaultRouter()
router.register(r'activities', ToDoViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    # path('requests/update/<int:pk>', CreditRequestUpdateView.as_view(), name='requests_update'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]