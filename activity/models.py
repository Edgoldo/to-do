from django.db import models

class ToDo(models.Model):
    """Model definition for ToDo."""
    content = models.TextField()
    is_favorite = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Unicode representation of ToDo."""
        return self.content
