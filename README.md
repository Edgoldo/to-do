# To Do

List of to do activities that can be marked as finished or select favorites.

1) Requirements
---------------

This project runs on Linux, it requires the last verstion of python and pipenv, to install pipenv, can use the next command:

$ python3 -m pip install pipenv --upgrade

Then install the requirements of the environment, to this project we use python 3.8:

$ pipenv install

After that, launch the environment of the project:

$ pipenv shell
(to-do) $

2) Run the project
------------------

To start the project on a server, you need to setting the database values:

On the credit_approval folder, you must rename the default_settings.py to settings.py and then go to the DATABASES variable:

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'DATABASE_NAME',
        'USER': 'DATABASE_USER',
        'PASSWORD': 'DATABASE_PASSWORD',
        'HOST': 'localhost',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True, # Create transactions on each view request
    }

Change the value of the keys NAME, USER and PASSWORD to your own database configurations.

Then can runs the django commands to migrate the database:

(to-do) $ python manage.py migrate

And finally you can run the server project:

(to-do) $ python manage.py runserver

3) Make request to the project urls
-----------------------------------

At this moment the available urls are:

    "activities": "http://localhost:8000/activities/",

And the API documentation is available in:

    "docs": "http://localhost:8000/docs/"
